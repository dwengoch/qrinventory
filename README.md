# Dwengo Helvetica #

Dwengo Helvetica is a non-profit Swiss association. Our goal is to spread the passion for science and technology among young people and adults without a technical or scientific background. We organize interdisciplinary activities merging robotics, electronics, computer programming, physics, math, and art.

### QR Inventory ###

This repository contains development files for a simple inventory management using QR codes.

* The command line interface is written i python 2.7 using zbar and sqlite3.
* The GUI is written using PyQt5

### Contributions ###

You contributions are welcomed!