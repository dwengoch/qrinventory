#!/usr/bin/python
# -*- coding:utf-8 -*-

'''
Copyright (C) 2015 - Juan Pablo Carbajal
Copyright (C) 2015 - Dwengo Helvetica

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.


@author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>
'''

import sys
import sqlite3 as lite

from itemscanner import QRitemscanner
from zbar import WindowClosed
from string import Template

from PyQt5.QtWidgets import QWidget, QTabWidget, QLabel, QApplication,\
                QDesktopWidget, QGridLayout, QMainWindow, QPushButton, QTableView
from PyQt5.QtGui import QIcon
from PyQt5.QtSql import QSqlTableModel, QSqlDatabase

class MainWindow(QMainWindow):
    def __init__(self):
        super(MainWindow,self).__init__()
        self.initUI()

    def initUI(self):
        # Show the name of the DB
        self.inventory_db = 'test.db'
        self.sBar = self.statusBar()
        self.sBar.showMessage(self.inventory_db)

        self.resize(800, 600)
        self.center()
        self.setWindowTitle("Dwengo QR Inventory")
        self.setWindowIcon(QIcon("DwengoQRInv_icon.png"))

        # --- TABS --- #
        tab_widget = QTabWidget()
        self.initInventoryTab(tab_widget)
        self.initReserveTab(tab_widget)

        self.setCentralWidget(tab_widget)
        self.show()

    def center(self):
        qr = self.frameGeometry()
        cp = QDesktopWidget().availableGeometry().center()
        qr.moveCenter(cp)
        self.move(qr.topLeft())

    def initInventoryTab(self, widget):
        tab = QWidget()
        grid = QGridLayout(tab)
        grid.setVerticalSpacing(1)

        # Buttons
        btn_Add    = QPushButton("Add", self)
        btn_Remove = QPushButton("Remove", self)

        # Events & Signals
        btn_Add.clicked.connect(self.scanItems_inv)

        grid.addWidget(btn_Add, 1,0)
        grid.addWidget(btn_Remove, 2,0)

        # Table view
        db = QSqlDatabase('QSQLITE')
        db.setDatabaseName(self.inventory_db)
        db.open()
        self.model = QSqlTableModel(self, db)
        self.model.setTable("Inventory")
        self.model.select()
        self.table = QTableView()
        self.table.setModel(self.model)
        self.table.show()
        grid.addWidget(self.table, 1,1,5,1)

        widget.addTab(tab, "Inventory")

    def initReserveTab(self, widget):
        tab = QWidget()
        grid = QGridLayout(tab)

        btn_Add = QPushButton("Scan", self)

        grid.addWidget(btn_Add, 1,0,1,2)

        widget.addTab(tab, "Reserve")


    def scanItems_inv(self):
        s = QRitemscanner()
        con = lite.connect(self.inventory_db)
        with con:
            cur = con.cursor()
            query = Template('INSERT INTO Inventory (serial,name) VALUES($serial,"$name")')
            while True:
                try:
                    s.decode_webcam()
                    if not s.data.keys():
                        raise ValueError('No QR code visible.')
                    else:
                        try:
                            print query.substitute(s.data)
                            cur.execute(query.substitute(s.data))
                            self.table.show()
                        except lite.IntegrityError:
                            print 'Item already in DB'
                except WindowClosed:
                    s.destroy()
                    break

if __name__ == '__main__':
    app = QApplication(sys.argv)
    ex = MainWindow()
    sys.exit(app.exec_())

## vim: set expandtab tabstop=4 :
