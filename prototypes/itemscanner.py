#!/usr/bin/python
# -*- coding:utf-8 -*-

'''
Copyright (C) 2015 - Juan Pablo Carbajal
Copyright (C) 2015 - Dwengo Helvetica

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.


@author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>
'''

import os
import zbar
import sqlite3 as lite
import sys
import datetime
import time

from string import Template

def getVideoDevices():
    if os.path.exists("/dev/v4l/by-id"):
        for dev in os.listdir("/dev/v4l/by-id"):
            try:
                yield([
                    " ".join(dev.split("-")[1].split("_")),
                    os.path.join("/dev/v4l/by-id", dev)
                ])
            except:
                yield([
                    dev,
                    os.path.join("/dev/v4l/by-id", dev)
                ])

class QRitemscanner ():

    def __init__(self):

        # Get webcam
        device = [v for v in getVideoDevices()]
        if len(device) > 1:
            # TODO: dialog to select device
            raise NotImplemented
        elif len(device) == 1:
            device = device[0]
        else:
            raise ValueError('No video device found.')
        self.device = device[1]

        # create a Processor
        self.proc = zbar.Processor()
        # configure the Processor
        self.proc.parse_config('enable')
        # initialize the Processor
        self.proc.init(self.device)
        self.data = None
        # setup a callback
        def parse_dict(proc, image, closure):
            # extract results
            for symbol in image:
                if not symbol.count:
                    self.data = dict([tuple(x.split("=")) for x in symbol.data.encode(u"utf-8").split()])
                    time.sleep(0.1)

        self.proc.set_data_handler(parse_dict)

        ## enable the preview window
        self.proc.visible = True

    def decode_webcam(self):
        # initiate scanning
        self.proc.active = True
        try:
            self.proc.process_one()
        except zbar.WindowClosed:
            raise

    def destroy(self):
        del self.proc
