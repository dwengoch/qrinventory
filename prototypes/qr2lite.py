#!/usr/bin/python
# -*- coding:utf-8 -*-

'''
Copyright (C) 2015 - Juan Pablo Carbajal
Copyright (C) 2015 - Dwengo Helvetica

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.


@author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>
'''

import os
import zbar
import sqlite3 as lite
import sys
import datetime
import time

from string import Template

def getVideoDevices():
    if os.path.exists("/dev/v4l/by-id"):
        for dev in os.listdir("/dev/v4l/by-id"):
            try:
                yield([
                    " ".join(dev.split("-")[1].split("_")),
                    os.path.join("/dev/v4l/by-id", dev)
                ])
            except:
                yield([
                    dev,
                    os.path.join("/dev/v4l/by-id", dev)
                ])

def decode_webcam(device='/dev/video0'):
    # initiate scanning
    proc.active = True
    try:
        proc.process_one()
    except zbar.WindowClosed:
        raise

device = [v for v in getVideoDevices()]
if len(device) > 1:
    # TODO: dialog to select device
    raise NotImplemented
elif len(device) == 1:
    device = device[0]
else:
    raise ValueError('No video device found.')

# create a Processor
proc = zbar.Processor()

# configure the Processor
proc.parse_config('enable')

# initialize the Processor
proc.init(device[1])

data = dict(string=None,typ=None)
# setup a callback
def my_handler(proc, image, closure):
    # extract results
    for symbol in image:
        if not symbol.count:
            data['string'] = symbol.data.encode(u"utf-8")
            data['typ'] = dict([tuple(x.split("=")) for x in data['string'].split()])

proc.set_data_handler(my_handler)

## enable the preview window
proc.visible = True

# connect to database
con = lite.connect('test.db')

#table name
d = datetime.date.today()
name="Workshop_{m}_{y}".format(m=d.month,y=d.year)
with con:

    cur = con.cursor()
    cur.execute("DROP TABLE IF EXISTS {}".format(name))

    query = 'CREATE TABLE {}(id INT PRIMARY KEY, name varchar(255))'.format(name)
    cur.execute(query)
    query = Template('INSERT INTO {n} VALUES($id,$name)'.format(n=name))
#    import pdb
#    pdb.set_trace()

    while True:
        try:
            decode_webcam(device=device[1])
            if not data['string']:
                raise ValueError('No QR code visible.')
            else:
                try:
                    cur.execute(query.substitute(data['typ']))
                except lite.IntegrityError:
                    print 'Item already in DB'
                    time.sleep(0.2)
        except zbar.WindowClosed:
            break

## vim: set expandtab tabstop=4 :
